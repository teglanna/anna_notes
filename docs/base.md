---
id: base
sidebar_label: link together
title: How to connect those three terms
---

|Stages  | Í.| ÍÍ.  | ÍÍÍ.
|---|---|---|---|---|
| Diagnosis – naming |Ca. Anx.| Co. Di.  |Ne. Cap.|
|Phase, Active-passive | Observation, passive  | Accept – letting go part, “prethink”, predict, beenged újat a megfigyelés által  | Active, see target, set up training program, to change mindset
| Symptoms – story telling, topics   | Mind-body dualism, body-outside dualism  | Previous, new ability together, stress, fears, emotions, autistic disorder  | Introduce dedicated moment, train mind, more practical part  |
| Treatment – practice | Step out, accept different viewpoints from outside → | Learn how to let previous, old belief letting go → | When new belief, attained flexibility has to be trained, keeping on level, methods: premortem, meditation, bawling, mantras...

It means it aims to describe the circumstances, the process of changing an old, “outdated” belief/thought/approach, something with a new one, which can follow “current trends”.

There are three main chapters to cover those stages written in a story-telling way(?) which helps to explicate theme.
The first part, the pre table of content is the diagnosis part, where three definitions designate the three phases of the topic.

Diagnosis, naming the phenomenon


## I. Getting know (confrontation)

Cartesian anxiety

Descartes mind-body dualism embraces the idea that mind and body are distinct but closely joined. His discussion on embodiment set the agenda for the question:
“What exactly is the relationship of union between the mind and the body of a person?”
Cartesian anxiety refers to that notion and comes naturally from that dualism: people has suffered from a longing for ontological certainty. It includes the desire that the surrounding world should be able to handle as a separated entity from ourselves, therefore we can have an unchanging knowledge about ourselves and itself.

In the first chapter I’m going to investigate that phenomenon, how it is manifested in the daily life.
(I’m using elephant related “stories” arbitrary to fill a cavernous gap of experiencing India without the sight of an elephant.)


## II. Letting go

Cognitive dissonance refers to the situation where a person holds two or more contradictory beliefs or values which produces a feeling of mental discomfort and fosters to a modification of the attitude to reduce the created stress and restore the balance.

In the second chapter I’m investigating the circumstances where this phenomenon pops up.
Which kind of situations and experiences (inner can be too) can lead to this feeling.
What are the situations where you should modify your existing behaviour (with changing your mindset) for going through that certain situation. 
How to generate those situations in order to learn?


## III. Iterate over and over

Negative capability: a theory first articulated by John Keats in 1817 to characterise the ability of the greatest writers (particularly Shakespeare) when they follow an approach even it leads them into intellectual confusion and uncertainty. The term has been used later to describe the strength when someone is able to act and operate in spite of the unknown negative circumstances.
“At once it struck me, what quality went to form a Man of Achievement, especially in literature, and which Shakespeare possessed so enormously,” he wrote. “I mean Negative Capability, that is when man is capable of being in uncertainties, mysteries, doubts, without any irritable reaching after fact and reason.”



Content
I. Getting know	4
1. Ganesha and the leftover	4
2. Don’t think to the pink elephant & the big: hand check	5
3. Elefánt a porcelánboltban	6
II. Letting go	7
1. Eating meat, meeting eat	7
1. Emotional, visceral stuff	8
Animal Spirits in Ancient Medicine and Literature	8
3. Szent vagy, percussive practice for absolution	9
III. For thoughts in mind	10
1. Dedicated moment	10
2. Demystifying meditation	11
4. Excercises	13

