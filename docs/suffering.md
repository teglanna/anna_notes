---
id: suffering
title: Thoughts on suffering
sidebar_label: Suffering
---

Life is suffering.
The root of suffering is attachment. i
Descartes mind-body dualism embraces the idea that mind and body are distinct but closely joined.
His discussion on embodiment set the agenda for the question: “What exactly is the relationship of
union between the mind and the body of a person?” Cartesian anxiety refers to that notion and
comes naturally from that dualism: people has suffered from a longing for ontological certainty. It
includes the desire that the surrounding world should be able to handle as a separated entity from
ourselves, therefore we can have an unchanging knowledge about ourselves and itself.
When we meet with something new, which doesn’t fit into our worldview, however it “still works”
in the opposite way, we become insecure, and loose our balance, because we have to choose
between the two contradictory beliefs or values. Those two views are “living” in us at the same time
and we are going to change unintentionally, because we have learned something new. This is the
beginning phase of our cognitive process. (cognitive dissonance)
We adore the person, who is able to quickly adapt to the unexpected situations and the suddenly
happened changes.
John Keats says, Shakespeare is one of the people, who are not even adapt to the challenges but
seek those situations, even though it creates states, they can’t prepare before. It is a brave artistic
attitude, face the unknown just to learn and create something new. This ability is called negative
capability.



