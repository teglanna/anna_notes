---
id: outlines
sidebar_label: Outlines sok
title: Struggling with define and narrow topic
---

## Try to get the main clue

**why i am doing this**

because there was this virus stuff, "started" in February (2020), changed my (and others) mind and behaviour

### Research Methodology

* field work: see people, how they face issues -> how they solve it (or how can't solve it), what are they well-tried techniques
* make interviews: people from religious "background": buddhist, catholic,
*				   'others' and they beliefs
* daily journal + short videos

### Raw outline 1.0

* triggering event
* am I autist?
* negative capability + cognitive dissonance
* LE A PASSZIVITÁSSAL! ~ train (change) your mind!
* EMPTINESS - meaning of thing
* "bad" feelings -> transform into "good" feelings
* contemplative meditation - Jálics Ferenc(!),
```meditation as preventive medicine```
* "use" situation
* ~~dedicated~~ moment
* together|alone (risk communication)
* all about ```eating```