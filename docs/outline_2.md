---
id: outline_2
sidebar_label: Outline 2.0
title: Second try
---

## Adding questions

* How to transform negative event, gain a positive outcome?
* How to train our mindset to deal with (global) crisis?
* How to "use" untertainty to develop trust?

```/on the same page/
maitri
mudita
karume
```

<style>a{color:red;}</style>[upeksha](upeksha.md)
