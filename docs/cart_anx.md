---
id: cart_anx
sidebar_label: cartesian_anxiety
title: Cartesian Anxiety
---

Descartes mind-body dualism embraces the idea that mind and body are distinct but closely joined. His discussion on embodiment set the agenda for the question: “What exactly is the relationship of union between the mind and the body of a person?” Cartesian anxiety refers to that notion and comes naturally from that dualism: people has suffered from a longing for ontological certainty. It includes the desire that the surrounding world should be able to handle as a separated entity from ourselves, therefore we can have an unchanging knowledge about ourselves and itself.
