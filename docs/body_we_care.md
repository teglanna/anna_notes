---
id: body_we_care
sidebar_label: The body we care
title: The body we care notes + William James
---

## filtered sentences and others

*requires the concept of an active state of relation between body & mind*
how emotion affects.

* lovas_példa: Hans trust + interest -> focuses on the flutters of the body
unintentional movements
the rider thinks about the movements the horse should perform.
they have the power of intense contenctration in expectation
* rats -> how our belief affects our experiences
"Rats produced in a pseudo-reality", the unreal field of by-products of **beliefs, expectations & illusions.**"

-> concept of authority
Rosenthal's authority allows the students to be entitled to produce competent rats.

**who authorizes:**
*everything is shifting articulating many more things, giving chanced to many more entities to belong to the real world.*

the whole matter is a matter of:
- faith,
- trust
-  --> this is the way we should contrue the:
 	* role of expectations
 	* role of authority
	* role of events (that authorize and make things become)

### Beliefs - pragmatic definition:
 what makes, not what it is; what makes entities available to events

### Human experience

Our feelings dispose our bodies,
					 
                     our bodies dispose our feelings. (dispose=rendez)

Experiences making bodies and
					
                    bodies making experiences.
                    
We produce emotions and
			
            emotions produces us.
            
We are smiling because we are happy,
		
        or we are happy because we are smiling.
       
       (start to be...)
     
The experience of "making available"

dynamic relation between body and mind

### Body

- strange ambiguous sphere of being
```
 belongs sometimes to the world of objects outside.

				   to the world of subjects .inside
```