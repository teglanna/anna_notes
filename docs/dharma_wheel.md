---
id: dharma_wheel
title: A digested understanding on the twelve links
sidebar_label: Dharma wheel
---

![dharma wheel](assets/wheel_smaller.jpg)

1. Avijjā – Ignorance: Ignorance of the meaning of the Four Noble Truths “Not knowing suffering, not knowing the
origination of suffering, not knowing the cessation of suffering, not knowing the way of practice leading to the
cessation of suffering: This is called ignorance. It leads to action, or constructing activities.”
2. Saṅkhāra – Volitional impulses, karmic formations "These three are fabrications: bodily fabrications, verbal
fabrications, mental fabrications. These are called fabrications."
3. Viññāṇa – Sensual consciousness (The monkey represents our consciousness, the way we tend to spring from one
thought to another in an uncontrolled manner.) “These six are classes of consciousness: eye-consciousness, ear-
consciousness, nose-consciousness, tongue-consciousness, body-consciousness, intellect-consciousness.”
4. Nāmarūpa – Name-and-Form (body and mind) ”Feeling, perception, intention, contact, and attention: This is
called name. The four great elements, and the body dependent on the four great elements: This is called form."
5. Saḷāyatana – Six-fold sense bases "The eye-medium, the ear-medium, the nose-medium, the tongue-medium, the
body-medium, the intellect-medium."
6. Phassa – Contact “The coming together of the object, the sense medium and the consciousness of that sense
medium[note 12] is called contact.”
7. Vedanā – Feeling, sensation
8. Taṇhā – Craving ("thirst") "These six are classes of craving: craving for forms, craving for sounds, craving for
smells, craving for tastes, craving for tactile sensations, craving for ideas. This is called craving."
9. Upādāna – Clinging and grasping
10. Bhava – Becoming (karmic force, similar to volitional formations),
11. Jāti – Birth (arising of feeling of distinct self) "Whatever birth, taking birth, descent, coming-to-be, coming-
forth, appearance of aggregates, & acquisition of [sense] media of the various beings in this or that group of
beings, that is called birth."
12. Jarāmaraṇa – Old age and death "Whatever aging, decrepitude, brokenness, graying, wrinkling, decline of life-
force, weakening of the faculties of the various beings in this or that group of beings, that is called aging. Whatever
deceasing, passing away, breaking up, disappearance, dying, death, completion of time, break up of the aggregates,
casting off of the body, interruption in the life faculty of the various beings in this or that group of beings, that is
called death." (en.wikipedia.org/wiki/Pratītyasamutpāda, rigpawiki.org, )

