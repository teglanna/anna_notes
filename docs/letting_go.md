---
id: letting_go
sidebar_label: Letting go
title: Letting go
---

## Eating (the animal) (and eating at all…)
Investigation, action and result → dissonance, what about learning and experiencing?
Body observation with food (dropping food to the machine) Beledobom a gépbe, mit ad ki. Taking the first step inside (out)
###  Eating meat, meeting eat
“The very strange thing is that only a few years later, I, too, found the pressures of daily life in American society so strong that I gave up on my once-passionate vegetarianism, and for a while all my intense ruminations went totally underground. I think that the me of the mid-sixties would have found this reversal totally unfathomable, and yet the two versions of me had both lived in the very same skull. Was I really the same person?”[Hofstadter]

Unwanted longings. 
The main question is not about flesh it is about eating at all. The relation between me and the food. What do I want to “gain” from food?
To understand my desire and wish, I have to talk to my guts and to my stomach.

How much important is eating meat?
What is the difference between meat and flesh?
Hofstadter admit about his attitude of eating meat and being vegetarian.
Thinking about eating (meat) took too much energy and attention from my life. So I had to investigate it and understand this longing. But also understand how eating can become to an addiction.
Here I’m just talking about my struggling with the right decision which is proper for my beliefs.
According to my “previous” belief, that my blood type and lack of iron and vitamin B12 I had to consume flesh regularly. 
I couldn’t really investigate the origin of that longing until I tried to communicate to my guts.
To reach that signal I had to try not to eat for a day. It wasn’t that much difficult as I predicted.

After learning the signs of my digestive tract I gained a view of meat.
I have found the next points:
    • meat eating gives power
    • that power is rawer than the other
    • meat eating creates addiction with eating at all
    • it increases the hunger (in every sense)
    • it increases animal instinct
    • it mixes my feelings

Eating less, no meat, just few, in the morning:
    • too much fire
    • increased activity, has to be controlled

Eating as an addiction
A ritual to have and prepare pretty meal. Without cheese, meat, and many different ingredients, with less taste it is easier to focus on the target of the day.

1. Emotional, visceral stuff
just notes
Decartes tells about animal spirit in human body:
“These animal spirits are roaming fluids circulating rapidly around the nervous system between brain and muscles, and served as a metaphor for feelings, like being in high or bad spirit.”
The first use of the notion animal spirits is described by Descartes, Newton and other scientists on how the notion for the vitality of the body is used.
In this sense when I’m having animal soul with its fears too, it generates vitality in my blood? Maybe it increases the iron and vitamin B12 in my blood.
How can I connect to my (animal) spirit, does it depend on eating meat?
Where do emotions live?
 “An emotion ‘as nothing but the feeling of a bodily state, and [which] has a purely bodily cause’  – we do not cry because we are sad, we are sad because we cry.
A being that both produces emotions and is produced by them. An emotion is not what is felt but what makes us feel.” (The body we care, 127)

Animal Spirits in Ancient Medicine and Literature 
The technical concept of spiritus animalis can be traced as far back as 300 B.C., in the fields of human anatomy and medical physiology. There, animal spirits applied to the fluid or spirit present in sensory activities and nerve endings in the brain.
Thomas Hobbes used the phrase "animal spirits" to refer to passive emotions and instincts, as well as natural functions like breathing.
 William Safire explored origins of the phrase in his 2009 article "On Language: 'Animal Spirits'": 
The phrase that Keynes made famous in economics has a long history. "Physitions teache that there ben thre kindes of spirites", wrote Bartholomew Traheron in his 1543 translation of a text on surgery, "animal, vital, and natural. The animal spirite hath his seate in the brayne ... called animal, bycause it is the first instrument of the soule, which the Latins call animam." William Wood in 1719 was the first to apply it in economics: "The Increase of our Foreign Trade...whence has arisen all those Animal Spirits, those Springs of Riches which has enabled us to spend so many millions for the preservation of our Liberties." Hear, hear. Novelists seized its upbeat sense with enthusiasm. Daniel Defoe, in "Robinson Crusoe": "That the surprise may not drive the Animal Spirits from the Heart." Jane Austen used it to mean "ebullience" in "Pride and Prejudice": "She had high animal spirits." Benjamin Disraeli, a novelist in 1844, used it in that sense: "He...had great animal spirits, and a keen sense of enjoyment."[3]
The notion: animal spirit is used nowadays in economy in the context of decision making.
viscera: the large organs inside the body, including the heart, stomach, lungs, and intestines.
Humour: The term derives from the humoral medicine of the ancient Greeks, which taught that the balance of fluids in the human body, known as humours (Latin: humor, "body fluid"), controlled human health and emotion. 

3. Szent vagy, percussive practice for absolution
Utalás a kopogtatásos módszerre
just notes
It means: you are saint.
/No matter whether you eat meat or not, what are your beliefs./
(Because I have confessed my sin related meat eating I really need some absolution.)
One of my friend showed me a very simple technique, and I remember to it, because I didn’t understand the reason and the cause and effect around it. As I remember, when we met I was fairly sad and miserable and she told me to percuss my sternum and repeat the phrase: “You are saint.”
I don’t say I felt much better afterwards but it is still in my mind. The action and the sentence, I used for myself. How it is possible? Is it possible? Is it “legal”? Can I use and point this sentence on myself? Every time when I meet the possibility of eating meat, and I do it for some reason (because of the beliefs about B12 and iron supplement), I remember to this sentence.
One hand it tells me to accept myself with the existing hesitation and habit in order to understand and change it. On the other hand I have started to investigate the technique called EFT, which is about tapping meridian points to release painful emotions. I address my body directly with a sublime statement, even nothing special happens on the surface it helps me to recognize my corporeality again and again.
///You are what you eat.
I’m a fat, clever pig who likes useful dirtiness.
My visceral lets me know I still do need some animal inside me.
Bad and sad. Truly.
In my new frame of my mind I do not want to eat meat. Sadly I can’t state that I feel deeply compassion with the planned-to-killed animal. I know, I should hide this thought, but to be honest, I feel bigger grate when I take a leaf from my rosemary plant. I’m training my goodness with watching horrible, terrible videos about slaughterhouse and re-mind me and my gut, we should change our needs according to the better goodness.
Besides I’m practising being nice with my closest company, my family. 
It means I have “decided” to become occasional meat-eater, who meet with meat accompanied with the family and do it as a ritual (consume death person). With keeping this deflections I can change my old imprinting step-by-step, also can offer another viewpoint to my family(?)///
….
