---
id: envi
sidebar_label:  env(ironment)
title: Env(ironment)
---

*a collection of everyday differences in the context of previous travels(2017-2021)*

## What is an environment
<pre><code>
 • the aggregate of surroundings, conditions, influences, milieu, factors, context
 • ecologically: the air, water, and land in or on which people, animals, and plants live,
 minerals, organism, all other external factors surrounding and affecting a given organism
 at any time
 • the social and cultural forces that shape the life of a person or a population
 • the conditions that you live or work and the way that they influence how you feel or how
      effectively you can proceed
 • an indoor or outdoor setting that is characterized by the presence of environmental art
 that is itself designed to be site-specific
 • in computer science: the hardware or software configuration, or the mode of operation of
 the  computer system
</code></pre>

### Environmental factors:

* Are the given physical characteristics: temperature, water activity, pressure, radiation, oxygen, (lack of) nutrients, pH of water.
* Linked to space and place: can contribute to and reinforce socioeconomic and racial or ethnic health and lifestyle differences.
* A major motivation for the research on environmental determinants of health has been the repeated observation that many health outcomes are spatially patterned, plays determinate role in the health of a living organism.
* Artificial – built/community designed environment:
  * Human-made physical & social features
  * affects large groups that share common living or working spaces
  * land use mix, street connectivity, transportation systems:
     * harmful substances, such as air pollution or proximity to toxic site
     * access to various health-related resources (healthy or unhealthy foods, recreational resources, medical care)

### Environment variables:

Computer science: a dynamic-named value that can affect the way running processes will behave on computer.
How do I set an environmental variable.

### Abiotikus tényezők

Az abiotikus tényezők vagy élettelen tényezők a biológiában az ökológiai értelemben vett környezet élettelen, de az élethez szükséges fizikai és kémiai elemeinek, jelenségeit jelölő kifejezés. Általában komplexen, minden élőlényre hasonló mechanizmussal ható feltételek, mint a fény, a hőmérséklet, a levegő, az atmoszféra, a víz, a szél, illetve adott fajokra és ökoszisztémákra ható környezeti feltételek, mint a talaj, a szalinitás, a domborzati viszonyok és a természeti katasztrófák. A biotikus tényezőkkel (evolúció, szimbiózis, kompetíció stb.) együtt alkotják a természeti környezetet.
Az abiotikus környezet két irányból hat egy ökoszisztéma stabilitására (vagy instabilitására): egyrészről biztosítja az élet fennmaradásához szükséges feltételeket (például hőmérséklet, víz), másrészről a populációnak alkalmazkodnia kell az általa biztosított feltételekhez (például szélsőséges hőmérséklet, vízhiány). Az abiotikus tényezők legcsekélyebb állandósuló (például
talajerózió) vagy időszakos (például árvíz) változása is instabillá teheti és adaptációra, akklimatizációra készteti az adott ökoszisztéma egyedeit.


**What I'm seeking for:**
* same things in different conditions
* tools, which are necessary for living somewhere-anywhere
* this is an example, a test how I can include pdf in markdown file
--- I would like to upload content quickly


