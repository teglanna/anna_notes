---
id: route
title: Suffering from not understanding
sidebar_label: Route diagrams
---

## Diagrams

Here i am trying to figure out how it is possible to change an existing "old fashioned" thought, and use a new one.
The main presumption is that there is an unknown event we face, and the existing belief and behavior don't work anymore to go through.


### triggering event
![route_2](assets/t_event.jpg)

### try i
![route_3](assets/ne_ca_1.jpg)

### try ii
![route_3](assets/ne_ca_2.jpg)

### try iii
![route_3](assets/ne_ca_3.jpg)

### result
![route_1](assets/ne_ca_result.jpg)


### bad thought - right thought
![route_3](assets/bad_right_thoughts.jpg)

