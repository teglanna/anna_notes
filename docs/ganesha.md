---
id: ganesha
sidebar_label: Ganesha
title: Ganesha, the pink elephant
---

The pitfalls of cognitive dissonance

Don’t think to the pink elephant.

~~I was angry and very angry. I had to go to the bathroom during the night and I created an adventure for myself. Don’t let your fresher mind to play with itself and flood you with its surrealistic surfing.
I had a challenge focus only one certain sentence and exclude or don’t pay attention to the others. It made me so angry that I couldn’t fall asleep again.
 I started to try to catch my thoughts at their heads and it woke me up too much. Because it generates fears. There are triggering thoughts. When it is told you “don’t think to the pink elephant!” - sure you will think on it. And when I tell you: if you think to the pink elephant, then just “please forget to remind it to the purple frog, please, because that’s the worst.”
For sure you are going to remind both of them. Or if I didn’t tell that you are going to, maybe you won’t but anyway, now we are talking much enough about it to generate a stupid-long context to it to create an unimportant path in your brain to reach this entry. (ezt jobban meg kéne fogalmazni)
So fears. The fears that my “bad” thoughts will happen sometimes.
I think those fears “have reason for living” in the sense that you are able to “get there”, if you heard for example too much, that smoking leads to lung cancer you can have those kind of fears, or other type if diseases. Especially disease are in our  world of potential fears a lot.
Because we believe that we are independent from our health and diseases, we cant really affect, influence, control. Independent from that so our future depend on our body decision whether we are sick or not.
We are separated.~~

Ő maga is egy creatív gyorsmegoldással kapott új fejet, hogy életben maradjon. Muszáj volt neki nézőpontot váltania. Az elefántfej volt éppen akkor elérhető távolságban. Szerencsére, mert az elefánt bölcsességet is jelképez egyben. Érthető, hiszen a másként szemlélés a bölcsesség, megértés, együttérzés alappillére. Hállelúja!
Ganesha's dharma and his raison d'être is to create and remove obstacles.
The most well-known story about his birth tells that Parvati created a man from clay to protect her until she was taking bath. When Shiva, her husband arrived back, Ganesha didn’t recognized his father and he didn’t let him approach Parvati, so Shiva cut his head. When he got known about his newly born son, he replaced Ganesha’s head with the animal, which arrived first. It was fortunately an elephant, which is famous its wisdom.
 Ganesha, who lost his head has to get a new one immediately to survive. Ganesha inherited all of his fathers knowledge and as an extra he had to learn how to do a paradigm shift. He encountered a cognitive dissonance at that situation.
He is the protector of the new adventures, devotees believe that if Ganesha is propitiated, he grants success, prosperity and protection against adversity, he is the remover of obstacles.
His father is known of his destructive nature. So after he destroyes the old and untenable situation a new viewpoint has to be born, a paradigm shift.
According to this story I have associated to another personal notes from a night before.
I’m angry and very angry.
When I’m waking up to go to the toilet during the night I have many-many-bunch of thoughts storm my mind. It annoys me a lot when I have just decided to don’t let to enter the wave of those surrealistic fresher-mind-jumping-associations into my reality and focus on one certain sentence or a word. The problem is that my angriness just increases with the effort I freshly created for myself for that short period at 2am, and I’m just more nervous. co.di.
When they say, don’t think on the pink elephant.

The elephant who has to change his body to a human one.
Because of a small conflict with his father, Shiva.
By the way, Shiva is known of his destructive nature. After he destroys the old and untenable situation a new viewpoint should have been born as a paradigm shift.

Nice to have a phrase to describe anything. It apparels phenomenons with a meaning, which  can have its own life toward.
Nagyon klassz, hogy mindenre megvan a megfelelő kifejezésünk. Így szükség esetén folytathatjuk a kategorizálgatást, és a dolgok megkapják a létüknek ugyan nem feltétlenül járó jogosultságot és ami még fontosabb: figyelmet.

Cognitive dissoncance refers to a situation involving conflicting attitudes, beliefs or behaviors. This produces a feeling of mental discomfort leading to an alteration in one of the attitudes, beliefs or behaviors to reduce the discomfort and restore balance. It occurs when a person holds two or more contradictory beliefs, ideas, or values, or participates in an action that goes against one of these three, and experiences psychological stress because of that.
So it means, that sometimes it turns out that you should or could change your existing routine and behaviour to go through a situation more efficient and you have to change your mind, therefore you are stressed.
What if I generate directly these situations? For example I’m trying to do NOT think on a certain thought.

- eating meat

Cognitive dissonance coming from inner conflicts:
effort to change existing, frequently appearing thoughts, transform them to other ones.~~


## na még egyszer! Ganesha and the leftover
Separated head and body foreign body – phantom body feeling
The story
He is one of the most famous and respected god in the Hindu religion.
According to the most believed story he was created by Parvati to protect her until she was taking bath. When her husband, Shiva arrived back, Ganesha didn’t allowed him to approach Parvati, as he didn’t know his father. Shiva also didn’t recognize his son, and he cut his head accidentally.
When he learned of his mistake, he had to be creative and replace Ganesha’s head  immediately with the head of the first arriving animal. (Even they are gods, still need head to survive…)
It was an elephant (fortunately). An elephant which is the animal well-known about its memory also represents wisdom.
*TODO Ganesha changed his mind unconsciously. (azt, hogy miért, minek)
////Is it understandable, because different viewpoint is the basis of wisdom, understanding and compassion? Neee...////
Ganesha's dharma and his raison d'être is to create and remove obstacles.
He is the protector of the new adventures, devotees believe that if Ganesha is propitiated, he grants success, prosperity and protection against adversity.
“From that time” people salute him first, the representative of the new beginning and enterprise.
 Ganesha, who lost his head had to get a new one immediately to survive. Ganesha inherited all of his fathers knowledge and as an extra he gained a new viewpoint with the new head. 

His father is known of his destructive nature. So after he destroys the old and untenable situation a new one has to be born.
He is that “cool” as he already has to use a new head to continue his life. As he became half elephant he remembered his father knowledge already and he could integrate it into a bigger perspective/picture, gained a pure wisdom. It was a necessary act of course; living without a head is  inconceivable – neither in the world of gods. He was very pragmatic in the sense of doing spontaneously, (doing, action) without overwhelmed thoughts.
He was writing down Mahabharata after Vyasa was reciting it uninterruptedly so he had to understand and write, he was in rush…He was in rush and his pen was broken but he just broke off one of his tusk loosely so that the transcription could proceed without interruption, permitting him to keep his word. 

Ganesha’s shadow
There is a leftover from a human head and an elephant body.
This “guy” doesn’t really know how to use his human head with his elephant body.
He acts like a bull in a China shop. He is still desperate from the accident was happened with him.
He wants help from outside because he can’t find it inside. He lives in constant fears if another unknown event happens, he can’t control the future at all…

![ganesha](assets/ganesha.png)