---
id: getting_know
sidebar_label: Getting know
title: Getting know
---

## Elephant related stories
1. Ganesha and the leftover
Separated head and body foreign body – phantom body feeling
The story
He is one of the most famous and respected god in the Hindu religion.
According to the most believed story he was created by Parvati to protect her until she was taking bath. When her husband, Shiva arrived back, Ganesha didn’t allowed him to approach Parvati, as he didn’t know his father. Shiva also didn’t recognize his son, and he cut his head accidentally.
When he learned of his mistake, he had to be creative and replace Ganesha’s head  immediately with the head of the first arriving animal. (Even they are gods, still need head to survive…)
It was an elephant (fortunately). An elephant which is the animal well-known about its memory also represents wisdom.
* TODO Ganesha changed his mind unconsciously. (azt, hogy miért, minek)
////Is it understandable, because different viewpoint is the basis of wisdom, understanding and compassion? Neee...////
Ganesha's dharma and his raison d'être is to create and remove obstacles.
He is the protector of the new adventures, devotees believe that if Ganesha is propitiated, he grants success, prosperity and protection against adversity.
“From that time” people salute him first, the representative of the new beginning and enterprise.
 Ganesha, who lost his head had to get a new one immediately to survive. Ganesha inherited all of his fathers knowledge and as an extra he gained a new viewpoint with the new head. 

His father is known of his destructive nature. So after he destroys the old and untenable situation a new one has to be born.
He is that “cool” as he already has to use a new head to continue his life. As he became half elephant he remembered his father knowledge already and he could integrate it into a bigger perspective/picture, gained a pure wisdom. It was a necessary act of course; living without a head is  inconceivable – neither in the world of gods. He was very pragmatic in the sense of doing spontaneously, (doing, action) without overwhelmed thoughts.
He was writing down Mahabharata after Vyasa was reciting it uninterruptedly so he had to understand and write, he was in rush…He was in rush and his pen was broken but he just broke off one of his tusk loosely so that the transcription could proceed without interruption, permitting him to keep his word. 

Ganesha’s shadow
There is a leftover from a human head and an elephant body.
This “guy” doesn’t really know how to use his human head with his elephant body.
He acts like a bull in a China shop. He is still desperate from the accident was happened with him.
He wants help from outside because he can’t find it inside. He lives in constant fears if another unknown event happens, he can’t control the future at all… 

2. Don’t think to the pink elephant & the big: hand check
(body-mind dual. Nem szigorú, inkább body-line borderline investigation)
Trick to check whether you dream or not

Just for referring back to my favorite animal, to elephant I would like to point to the first contradictory phenomenon. When you are told to don’t think to the pink elephant, probably that will be your first thought. (Also you can just create a “bad association chain” with that elephant, let’s say don’t think to that pink elephant, because the purple frog will jump into your soup! Than you have two animals to exclude from your mindset.) When I’m discussing it with my friend, she points to me that there is a “real” story/ an existing tale about a king and the green elephant, so I’m using the wrong colour. Here at this point I won’t change it because the animal can be different as well, such as a tiger to represent something we have to avoid or feel fear. Or it can be anything else that an animal, but I like when it is a living being to show it is “movable” or agile itself. Or we like to think it is an independent existence can move easily inside in our head…

Few months ago I have learned a useful technique to diagnose whether I’m currently dreaming or not. When you suspect that the situation you experience is not real, you can take a look at your hands. If those are distorted, bigger, somehow abnormal with blurry fingers growing from it, you can be sure, you are currently sleeping and creating events in your mind only. When you recognize it you can decide whether you want to continue or exit, also you can influence the happenings consciously.
That’s fearful! And amazing at the same time.
Because that is the point which apparels you with responsibility. 
You recognize that your thoughts can turn into reality easily. Usually when I reach this “level” I start to afraid. The most fearful thoughts pop up into my mind and I have to control them if I don’t want to fight with bunch of zombies.

just notes
* TODO observe thoughts and their origin, vagy ne, lehet nem fontos. Hülye hasonlat
- erőszakkal gondolatot váltani
*****Actually in the real life we create our experiences indirectly from our previous experiences through our thoughts. When I want to control my thoughts and reteach or rework those, change it with others it is very complicated. It seems that it is very complicated. 
Thoughts have raison d'etre but you can change that “reason” (which points to the belief).
I’m trying to control my thoughts I have those fears visiting me. The fears that my thoughts can happen. It is a big deal. An enormous responsibility.
Usually we have fears related with disease, death etc.
How to handle those fears? How to don’t think to the pink elephant?
Controlling thoughts? Is not possible? first just pay attention!

3. Elefánt a porcelánboltban
Physical Examination – how to use it here
1. inspection – vision, smell, hearing, color, size…
2. palpation – touch with different parts of hand, varying degrees of pressure → asses for temperature, pulsation, masses, texture
3. percussion – organ is solid or filled with fluid or gas
4. auscultation – listening sounds, lower sounds – stethoscope
5. breathing
Határok, mit akarok ezzel? Starting the observation with the body
Like a bull in a china-shop
Social acting