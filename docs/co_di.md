---
id: co_di
sidebar_label: cognitive_dissonance
title: Cognitive Dissonance
---

Cognitive dissonance refers to the situation where a person holds two or more contradictory beliefs or values which produces a feeling of mental discomfort and fosters to a modification of the attitude to reduce the created stress and restore the balance.