---
id: iterate_over
sidebar_label: For thoughts in mind
title: For thoughts in mind
---

## Loop for health
Meditation as method
Breathing – connect with animal spirit, animus, Direct contact with our body
“But as things are, we have so far beset ourselves with rules, and concepts, and ideas of what must be done to make a building or a town alive, that we have become afraid of what will happen naturally, (and convinced that we must work within a “system” and with “methods” since without them our surroundings will come tumbling down in chaos.)”(C. A, Th T W, 14.o.)
1. Dedicated moment
just notes

State, stage, phase, process, 
progress, moment, ready, 
unfinished, stable, unstable…


Some states are more important than the others. (or we decide it is)
Particle owns its dedicated place in a certain time and then a certain order takes shape.
Particle, as its name shows, can be smaller or bigger, can consist of other particles, and decay to another ones as well. (zoom in and out  - pattern languages)
Also it is possible to create one with colliding others.
illustrate the diversity of winding path, the size and the life cycle of a building block.
The event and the grabbed moment can’t be separated, those give the right picture together of the state of a given particle.

Thinking about Higgs boson
- After collision there is an archaeological work to decrypt the routes are taken.
- Higgs boson can decay into photon.
- Higgs boson can decay into W boson → down quarks.
We need a certain state of a particle to “prove something”.
- Higgs boson is btw unstable.
- Higgs gives the mass!
Tömeget ad a higgs térben átvonuló részecskéknek, és maga a higgs tér átmeneti sűrűsödése is tömeget generál.
Stable unstable, mass, massless, massive.
Indirect identification, catch a moment, measuring momentum.
- Certain order takes shape.
- Certain particles are more important than the others.
Premature position report (idő előtti), early joy.
Some particles never decay…
decay. Energy is transforming into something else.
- Our desired moment is to see a Higgs boson.
- Our desired particle is Higgs boson.
Even if the Higgs boson consist of four quarks.
Higgs is Higgs, few minutum later it is quarks.
After a tiny fragment of a momentum it has changed to something else.
Before a fragment of a momentum it has been a Higgs boson, and “more before” it was a speeding proton. From the momentum it is possible to see its “past” and we can predict the direction, the possible future.

(momentum – a lendület az a mix, ami magában foglalja a megelőző tapasztalatokból szerzett különböző minőségű energiákat, aztán ebből az energiamintázatból, ennek alakulásából lehet megsaccolni a jövő alakulását)
	
Grabbing
Grabbed moment, frozen moment.
Grab from context: 
when you take a thing from its environment without the direct neighbours, and say, yes, it is!
Muons are important because you can conclude to a new particle was decaded into muon.
New particle was created.
Because the decay of the Higgs boson to bottom quark is so common.

We get the place/state/space before arrival. When it will arrive? When it seems it is stable enough.
(Bolyongás, flaneur, passzázs, eltévedés, tévelygés, helyzetközi helyzetjelentés.)

Secure states and secure arrival to that state, that’s only we want! (Ca. Anx.)
We put something to the formula which lasts for a tiny fraction of a second. (?)


2. Demystifying meditation

just notes
The most important research method I’m using during writing the thesis is meditation.
I don’t like to talk about meditation (too).
I don’t want to tell about the word, the meaning, about the origin.
I would like to talk about taking a big breath in and taking a big breath out.
It helps all the time.
Because it works. It is the first connection to my body.

Mit szerettem volna írni a meditációról?
Mindig szerenték írni róla, de valahogy nehezen megy.
Megfoghatatlan? Azért? Mi lehet az oka?
Mi az, ami miatt könnyen lebeszélem magam a “leülésről”?

Az a baj az egész “aktussal”, hogy ANNYIRA egyszerű.
A belégzés-kilégzés is.
Hogyan tudsz visszatérni mindig ugyanarra a pontra, ahonnan megfigyeled a gondolatokat?
Ez gyakorlat.
Ez gyakorlás.

A következőkben igyekszem olyan hasonlatokat találni, amik segítenek jobban körülírni a meditáció “mibenlétét”.

About zooming
Nem mindegy, hogy a betűméretet veszed nagyobbra, vagy jobban ráközelítesz a dokumentumra.
Ha a betű a nagyobb, akkor mások is jobban fogják olvasni a szöveget, ha a zoomolást választod, akkor az neked segítség abban az adott időszakban, amikor meg van nyitva a dokumentum. Ha ügyes vagy, akkor be tudod állítani, hogy legközelebb is a  kívánt méretben jelenjen meg a szöveg, vagy lehet, hogy állítani sem kell rajta… 

A Hungarian deaf-mute painter, called György Román.
In his paintings and writings the real life and the imagined picture of the dream are coming together.
He evokes experiences from his childhood, where he can refuge but can’t escape. It gives his immunity, the loyalty to the time of those experiences and it creates his vulnerability too.

According to his time dimension he glanced at the surrounding world from a certain distance, from the view of an “outsider”, he gained a more complex picture of the scenes, he was even able to depict a whole historical event, such as Invasion of Tartars.
When he portrayed a metropolis with a burning part, he showed the other parts (on the same scene) at the same time, giving the possibility to a presumption, a possible prognosis for the inhabitants’ future.
Thanks to this way of looking events become theatrical scenes, plays and circus, emphasising the circulation of different shapes and the decorations.
If we are talking about the lens of a moment, here the moment means something else.

In this bigger scale from his moment all event happens at once.

Alexander:











Miezelés

“And no sense at all in which what a system “ought to be” grows naturally from “what it is.” Take for an example, the atoms which a physicist deals with. An atom is so simple that there is never any question whether it is true to its own nature. Atoms are all true to their own natures; they are all equally real; they simply exist. An atom cannot be more true to itself, or less true to itself. And because physics has concentrated on very simple systems, like atoms, we have been led to believe that what something “is”, is an entirely separate question from what it “ought to be”; and that science and ethics can’t be mixed.” (C. A. 27. Th T W)

clinging
4. Excercises
    • practice: EFT tapping + palpation – with illustration
The cause of all negative emotions is a disruption in the body’s energy system.
It follows, then, that energy disruptions are the reason we have any kind of emotional issue like grief, anger, guilt, depression, trauma, and fear. Negative emotions come about because we are tuned into certain thoughts or circumstances which, in turn, cause our energy systems to disrupt.
Thoughts – body – emotion – thoughts – body – emotions…
Kopogtassuk meg a koponyát, mi van ott.
explanation how from Ca. Anx → Co. Di.
Experience first bodily → brand new thoughts
breathing
    • bawling
    • premortem
    • meditation