---
id: emo
sidebar_label: Emo and Animus
title: Emotional, visceral stuff
---

## animal spirit in human body
Decartes:
“These animal spirits are roaming fluids circulating rapidly around the nervous system between brain and muscles, and served as a metaphor for feelings, like being in high or bad spirit.”

The first use of the notion animal spirits is described by Descartes, Newton and other scientists on how the notion for the vitality of the body is used.
In this sense when I’m having animal soul with its fears too, it generates vitality in my blood? Maybe it increases the iron and vitamin B12 in my blood.
How can I connect to my (animal) spirit, does it depend on eating meat?
Where do emotions live?

```An emotion "as nothing but the feeling of a bodily state, and [which] has a purely bodily cause"  – we do not cry because we are sad, we are sad because we cry. A being that both produces emotions and is produced by them. An emotion is not what is felt but what makes us feel. (The body we care, 127)```

### Animal Spirits in Ancient Medicine and Literature

```The technical concept of spiritus animalis can be traced as far back as 300 B.C., in the fields of human anatomy and medical physiology. There, animal spirits applied to the fluid or spirit present in sensory activities and nerve endings in the brain.```

Thomas Hobbes used the phrase "animal spirits" to refer to **passive emotions and instincts**, as well as natural functions like **breathing.**

 William Safire explored origins of the phrase in his 2009 article "On Language: 'Animal Spirits'": 
```Physitions teache that there ben thre kindes of spirites``` wrote Bartholomew Traheron in his 1543 translation of a text on surgery, "animal, vital, and natural.

```The animal spirite hath his seate in the brayne ... called animal, bycause it is the first instrument of the soule, which the Latins call animam.```

**The notion animal spirit:** is used nowadays in economy in the context of decision making.
### viscera
the large organs inside the body, including the heart, stomach, lungs, and intestines.
### humour
The term derives from the humoral medicine of the ancient Greeks, which taught that the balance of fluids in the human body, known as humours (Latin: humor, "body fluid"), controlled human health and emotion. 

## soul poisons

air poisons:
- anxiety
- sorrow
- stress
- hurry-scurry
- wrath
- restlessness
- nervousness

fire poisons -> cause inflammation
- anger
- mettle
- betrayal
- wickedness
- envy
- rancour
- coarseness

water poisons:
- laziness
- bluntness
- sluggishness
- greed
- avarice
- clinging
