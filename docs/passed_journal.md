---
id: passed_journal
sidebar_label: Previous note for diploma
title: Daily notes for diploma writing
---

## Day 1 – 04/06
Main setup

    • create plan for the cycle (4 weeks)
    • create a mind map for the whole project: diploma thesis + work
    • set up daily journal
    • learning libre office features :)
    • define inquiry (vizsgálat)
    • define question and subqueries
    • define target audience
    • searching keywords
    • searching for TITLE
    • qualitative research: interviews? → mental health related

Today I’m looking back, what we have done on TDU last month.
Using the plan from there:

    • find a research question → which will be the title later
    • search for keywords with secondary research → collect reader → set up bibliography

Don Normant találtam ma :) Designer kritikus, a design 3 level

**Research question:**

How to re set our mind to be resilient, able to transform negative situations for learning, how can we program our brain?

Subqueries:
    • What is the relation between brain – heart – guts?
    • How can we deal with an epidemic on mental level?
        ◦ How to prepare for an epidemic or another catastrophe mentally?
    • What does it mean: mental power?
    • What is all about autism? Why is it different from other mental issues?


Régi jegyzeteket is szelektálok, kidobom a régi naplókat, ne kössenek.
Viszont találtam elég jó kis versikéket is.

Notes about catharsis + motivation
self-directed inquiry
personal inclination (hajlam, hajlandóság)
humankind

jó stílus:
Research inquiry: ​What motivates one to practice any form of catharsis and be productive?

Start with a citation: /quote/

“The function of imagination is not to make strange things settled, so much as to make settled things strange.”
​- G.K. Chesterton

mental health is a delicate topic

https://www.instagram.com/myeasytherapy/

![alt-text](assets/04_06.png)

## Day 2 – 04/07
Jegyzetek: **Valera Thomson: The embodied mind**

don’t forget: bawling (meghajlás)
habits, spaciousness of mind
panoramic awareness
training, grasping, delusion
maturity, prajna (paramita mantra) = érettség
no abstract description of experience itself
“a view from nowhere” (18)unlearning (42)
reflection meaning
who owns my body(75)
consciousness vijnana
apparent motion
aggregates are empty from self
emergent properties = kiemelkedő tulajdonságok
self-organizing capacity
emergence = felbukkanás, kialakulás

**Intro**
the embodiment of knowledge, cognition and experience
embodiment has this double
sense: it encompasses both the body as a lived, experiential structure
and the body as the context or milieu of cognitive mechanisms.
Hofstadter and Dennett's The Mind's Eye and Sherry Turkle's The
Second Self, meet with considerable popularity. 

Concept of nonunified decentered – buddhism
aim: create a bridge between cognitive science and human experience by using Buddhist traditions
human Self-understanding -intention
fear causes “depth-first processing”, focus by Don Norman, 3 levels of design
fear focus, negative capability → how could we use the power of those
happy, relaxed → can think out of box

11:38:
Hát egész eddig olvastam a tudományosabb szöveget:

Igen jó, és boldog vagyok, hogy értem, a neuroscience részét is egy darabon.
Viszont ahogyan leültem kicsit pihenni, kikapcsolni, megnéztem az idei elvonulás egyik bevezetőjét egy hollan fiatal sráctól.
Na az, az koronázta meg az eddig olvasottakat.
Neki sikerült megértetnie velem, mi az a emptyness.
Katartikus állapotba kerültem. Köszönet neki érte.

Közben rendeltem vitaminokat, mert bár értem, hogy tőlem függ, függök-e tőlük, de a hitrendszerem még nem engedi meg ezek elengedését. Magyarul úgy érzem, hogy hiányzik a szervezetemből. Drága Melinda! Erről még fogunk értekezni. Sokat. Meg remélem másokkal is.

Go back to literature
society, agencies: Tower Builder agent and subagencies
codependent arising - the Wheel of Life, causality
“Intention: resulting in the historical accumulation of habits, tendencies, and responses”(122)

![alt-text](assets/04_07.png)

## Day 3 – 04/08

Do something else,
prepare for going Home :)

Mireisz: Javaslap, március:

“(Csak zárójelben jegyzem meg, hogy az amerikai new age-es guru Dr Frederick Philip Lenz, azaz Rama egy interjújában hosszasan beszél arról, hogy szerinte a számítástechnikában alkalmazott gondolkodásmód nagyon hasonlít a zen buddhizmusban és az önfelismerés fejlettebb szakaszaiban használt gondolkodásmódra. A programozás segíti az embereket az elméjük fejlesztésében és szerinte segíteni fog a meditációban, amelyek a számítógép-tudományban használatosak, különösen a relációs adatbázis és a mesterséges intelligencia terén, melyek nagyon hasonlítanak a buddhista kolostorokban végzett gyakorlatokra. Hogy ez így van vagy sem, azt az olvasók döntésére bízom.)”

 Frame 3 research questions using the template one based on your previous TDU
 Peer Discussion – Discuss each person’s research inquiry in smaller groups
a. What is the relevance of the topic (to one’s discipline)?
b. Is the topic well defined?
c. Does the topic have adequate scope for research?
d. What could the potential outputs be?
e. What are the inter-disciplinary & Trans-Disciplinary Connections?
Learning about Thich Nhat Hahn

![alt-text](assets/04_08.png)

How to stay calm when you know you’ll be stressed?
Gary Klein psychologist: prospective hindsight (leendő utólagos előrelátás) = premortem

## Day 4 – 04/12

Folyt köv, válasz találása Húsvét derekán

Elvoltam itthon, mint a befőtt a húsvétolással, a kirándulással, főzéssel.
Amire jutottam a tézissel kapcsolatban az az, hogy mindet, amit csak nézek, látok, hallok, olvasok, forrásként fogok megjelölni.
Mire mutat rá a Húsvét?
Mai ted előadás: https://www.ted.com/talks/amy_cuddy_your_body_language_may_shape_who_you_are#t-373897
igen, hatalmi testi helyzet → stressz level, kortizol >, tesztoszteron <
Ferenc pápa, tévé film, mire jöttem rá?
Nem tudom, az csapta meg nagyon a fülemet, mikor arról esett szó, hogy mindenkivel ugyanannyira kedves, nem tesz kivételt. A kedvesség amúgy nem kerül pénzbe, és kifogyhatatlan eszköz.

## Day 5 – 04/13
Hétfő, tizenhárom

És amúgy meg Húsvétnak a hétfői napja.
Locsolásmentes, indokkal hál’ Istennek.

Belemerültünk anyával a XVI. Benedek & Ferenc pápa kapcsolatába.
Lásd bibliográfia!

## Day 6 – 04/15

Kétnaponta?
Itthon, Lehelen.
Pakolászás, forráskeresés

**Maps of Bodying, Erin Manning**
elhaladó testek, repülő effektus, roland barthes révén
amikor a repülőn ültem, és az atomjaim a nagyon gyorsan mozgó jármű kényére áttranszportálódtak a-ból bébe. Az érzet, azt kéne még körüljárni jobban-jobban…
think differently: prisoners (convivial tools 11.)

## Day 7 – 04/16
Suffering day, lack of energy

**Embodied mind** olvas
no independent pregiven world, our cognition and beliefs shape it
you’re expressing your own beliefs
common sense
“For less circumscribed or well-defined task domains, however, this
approach has proved to be considerably less productive. “
9. fejezet: adaptation, natural selection
fitness as a measure of abundance (bőség, jólét), measure of persistence
natural drift (sodródás, hajtás)
randomness
“... if a gene is actively selected, it will bring
along-in a "hitchhiking" effect-any others that are close enough.”
everyday commonsense realism?
Emptyness: 225.old
contemplative=elmélkedő

## Day 8 – 04/17

Still less energy but happy day with planting vegetables
Gardening meditation.
there is no methodological basis  for a middle way between  objectivism and subjectivism. - linked to representation
Hume: outer bodies
“no unique mapping between words and world” (related the word: empty)
contemplative=elmélkedő, attain=elér
“ The recognition that those
are empty of any actual existence manifests itself experientially as an
ever-growing openness and lack of fixation. An open-hearted sense
of compassionate interest in others can replace the constant anxiety
and irritation of egoistic concern.” (236) + term of freedom!
Objectivism & nihilism relation
"the death of God" is a dramatic statement of this collapse of fixed
reference points. Nietzsche 
meditators: self & others
unconditional, supreme state, spontaneous compassion
developing compassion (251)
ethical know-how

Collecting key insights, attractive topics, creating relations, maps to connect ideas

## Day 9 – 04/18

Titles of chapters
Overcast weather
Organizing references, outline, keywords

![alt-text](assets/topic_route_1.jpg)

Notes from **Upeksha lesson**:
Brahmavihara – four immeasurables:
maitri - loving kindness; mudita – empathetic joy
karuma – compassion, upeska  - equanimity 
What I want to (do)
    • something you don’t have but want
    • something you have but want to get rid of
    • something you have and want more of it

duties, necessities
having, maintaining a living environment – develop tools for conviviality
cognitive dissonance, its appearance in my life?
Stockholm syndrome
Change management
equalization, balance
feeling, recognition, perception – choice, consciousness
uncertain conditions
The Wheel of Dharma

## Day 10 – 04/20

Merging emerging thoughts & previous notes

**Jálics Ferenc könyvéből** jegyzetelés:

Introduction:
the Way to God.
From pre-contemplative to contemplative prayer
pre-contemplative: purification, changing
contemplative: Way of Unification (4.o.)
“During this initial state we have to make an effort. Paradoxically, we have to try hard not to try at all.
Nem érti a meditáció lényegét? Ezért használja a kontempláció szót…

Mi az ami zavar?

Ezt kell elmagyaráznom a tanaraknak holnap.
Az zavar, hogy tudományos és vallásos oldalról is bizonyos csövön keresztül szólnak az emberekhez. Akármennyire is bizonygatják ezekben a forrásokban, hogy a target audience gyakorlatilag bárki lehet, viszont a megfogalmazás és a szókészlet mindig egy bizonyos irányba mutat, viszont ez azért lehet, mert ők maguk nem tudnak jobban “nyitni” a lencsén?

Tudományos téren, ahogy nézem a “beles” tutorialt, és mindenféle kísérleteket, gyógykezeléseket bemutatnak, mindig az az érzésem, amit már a Biobanknál is éreztem, tapasztaltam, hogy az ember és a teste két külön dolog és majd “ők megoldják” mindenféle “átültetgetéssel” a problémát. Biztosan hatékonyak ezek a kezelések, viszont itt az ember megint csak egy passzív szereplő.

Anya: vastagbél: kiválasztás
Bíznom kell abban, hogy ki tudom választani spontán azt, amire szükségem van.
Mit jelent ez?
Azt, hogy amikor nézek egy tudományos videót, nem akarom foggal-körömmel az egészet megérteni, hanem hagyom, hogy annyi jöjjön át belőle, amennyit a szűrőm enged.
Mert nagyon sokszor nem ok nélkül nem veszem észre azt, amit “kellene”, hanem azért, mert bennem az csak mérget termelne…
Mi ez?
Hogy jön ez ide?

## Day 11 – 04/22

**Comfortable personalializationalialized note taking**

```Szent vagy```

It means you are saint.
No matter whether you eat meat or not, what are your belief system.
Is eating meat a key aspect of the life?
If I want to “go back” and ask other peoples opinion, I would start with Mr. Hofstadter.
I like his way of thinking.
Repeating these two words in your mother tongue helps you to understand that you are saint.
Does it degrade its meaning and deprive “most important, famous” saints from their glories?
I don’t think so. But actually you can tell it to your body. It is in second person singular, so if you are not enough comfortable with it, you can still tell it to your body, which is actually the same as you. Nice trick.
Eating meat. Meeting eat.
You are what you eat.
I’m a fat, clever pig who likes useful dirtiness.
In my beliefs it is too much engraved I need B12 and iron.
Eating  light, eating pork (instead of pig).
My visceral lets me know I still do need some animal inside me.
Bad and sad. Truly.
In my new frame of my mind I do not want to eat meat. Sadly I can’t state that I feel deeply compassion with the planned-to-killed animal. I know, I should hide this though, but to be honest, I feel bigger grate when I take a leaf from my rosemary plant. I’m training my goodness with watching horrible, terrible videos about slaughterhouse and re-mind me and my gut, we should change our needs according to the better goodness.
Besides I’m practising being nice with my closest company, my family. 
It means I have decided to become once-a-month meat-eater, who meet with meat accompanied with whose family and consume death person. With keeping this deflections I can change my old imprinting step-by-step, also can offer another viewpoint to my family(?)
b. With taking decision I gave a hit to my selection gastrointestinal tract.
Kiválasztó szervrendszer = excretory system

I helped to work properly to my excretory system, especially to my 
metabolic disorder
metabolism = anyagcsere

ruminant, rumination = kérődzés, töprengés, elmélkedés
gastrointestinal = 
intestine = bél

képtelen vagyok felvállalni saját magam.

## Day 12 – 04/24
Comfortable noting – folyt. köv.

Végre elkezdtem írni. Most meg a fejezetrendszerezés a gondom. Bazz…
Beastly flesh eating. Eating meat leads to something else. Your behaviour changes.
The beast is going to hunt. Instinct not intuition.

![alt-text](assets/breath.png)

## Day 13 – 04/29

**Where do emotions live?**

Ez egy szerda volt.
A korábbi napokat nem írom, mert nem voltak túl hatékonyak úgy emlékszem.
Szerdán olvastam Decartesról, és a Cartesian anxietyről.
Megállapítottam, hogy D. nem utasította el élből az empirikus gondolkodásmódot, csak máshová helyezte a hangsúlyt. Mondhatni az ember-természet konfliktra, mind-body határra, ezért is vettem őt elő.
- megemlíti azt a népszerű, akkori felfogást, miszerint az animal spirit a humoral systemen keresztül van áramoltatva a testben.
These animal spirits were believed to affect human soul.

Elővettem a **BioBankos, Foreign Bodies prezit** és elkezdtem az újat!
Breathing – connect with animal spirit, animus
Decartes:
Animal spirit in human body.
These animal spirits are roaming fluids circulating rapidly around the nervous system between brain and muscles, and served as a metaphor for feelings, like being in high or bad spirit.

### Animal Spirits in Ancient Medicine and Literature
The technical concept of spiritus animalis can be traced as far back as 300 B.C., in the fields of human anatomy and medical physiology. There, animal spirits applied to the fluid or spirit present in sensory activities and nerve endings in the brain.
	https://en.wikipedia.org/wiki/Animal_spirits_(Keynes)
The first use of the notion animal spirits is described by Descartes, Newton and other scientists on how the notion for the vitality of the body is used. 
Thomas Hobbes used the phrase "animal spirits" to refer to passive emotions and instincts, as well as natural functions like breathing.
 William Safire explored origins of the phrase in his 2009 article "On Language: 'Animal Spirits'": 
The phrase that Keynes made famous in economics has a long history. "Physitions teache that there ben thre kindes of spirites", wrote Bartholomew Traheron in his 1543 translation of a text on surgery, "animal, vital, and natural. The animal spirite hath his seate in the brayne ... called animal, bycause it is the first instrument of the soule, which the Latins call animam." William Wood in 1719 was the first to apply it in economics: "The Increase of our Foreign Trade...whence has arisen all those Animal Spirits, those Springs of Riches which has enabled us to spend so many millions for the preservation of our Liberties." Hear, hear. Novelists seized its upbeat sense with enthusiasm. Daniel Defoe, in "Robinson Crusoe": "That the surprise may not drive the Animal Spirits from the Heart." Jane Austen used it to mean "ebullience" in "Pride and Prejudice": "She had high animal spirits." Benjamin Disraeli, a novelist in 1844, used it in that sense: "He...had great animal spirits, and a keen sense of enjoyment."[3]
animal spirit & decision making (economy)
viscera: the large organs inside the body, including the heart, stomach, lungs, and intestines.
Humour: The term derives from the humoral medicine of the ancient Greeks, which taught that the balance of fluids in the human body, known as humours (Latin: humor, "body fluid"), controlled human health and emotion. 

Esti olvasás
Embodied Mind (16.o) jegyzet: (emotions & the body)
ambiguity James wants to produce or to preserve, does not appear at first glance. When he defines emotion ‘as nothing but the feeling of a bodily state, and [which] has a purely bodily cause’ (1890: 459) – we do not cry because we are sad, we are sad because we cry.
 it was not to define a passive affected being, but rather a being that both produces emotions and is produced by them. An emotion is not what is felt but what makes us feel. (17.)

Day 14 – 04/30
Délelőtti Nagesha-rajz és emberi szánalmasság


Délelőtt:
prezi folyt köv.
Ganesha + Nagesha integrate, gimpezéses illusztrálósb
Day 15 – 05/01
Könyvpakolok paralell és összegzek

- Videó a Duna partról, sima állóképek
- összegzem a heti jegyzeteket, prezit állítok, animal spirit és légzésről olvasok.
Megnéztem egy csudavideót egy bálnás képzőművésznőről.



Visszatérek az idegrendszerre és a testnedvekre

Descartes:
light and roaming fluids circulating rapidly around the nervous system between the brain and the muscles→ animal spirits live here, and served as a metaphor for feelings, like being in high or bad spirit.

Day 16 – 05/04
Akkor elkezdem

Ma végre(!)
Elkezdtem írni, végre leesett, hogy így is úgy is egy dokumentum lesz a végeredmény, minek szarakodok, írok, és nevet adva elmentem, aztán kész!

Letettem a kis fenekem és úgy tettem, mint aki ír.
És egy pár óra múlva valóban el is kezdtem.
Működik a pretend. Mindenkor, mindenhol. (pre-tend – elő tendál, előkészít)
Mosolyogj és vidám leszel! :)
A délelőtt folyamán fogyott a kávé, kétszer is volt hangyás kép, szóval kellett egy pár löket a mentálnak elindulni.
Aztán meg elkezdődött a szófosi, hál’ Istenke, mostmár csak szelektálnom (kellene) abból, amit valóban megosztok, hogy ne legyen tényleg minden leírva, ami a fejemben megfordul, mert az elég tré…
és zavaros.
És haszontalan.

Day 17 – 05/13
Szervác napja

Igen, több mint egy hete nem foglalkoztam a tézissel.
Eléggé motiválatlan voltam.
Tegnap, azaz május 12-én bemutattam a prezimet, amit kreáltam a témával kapcsolatban és délután beszélgettem Bernddel. Nem válaszol az emailemre, de jobb is így, ha direkt csinálja, főleg!

Most viszont igyekszem visszarázódni a kutatós, szakdogaírós hangulatba.
Először is olvastam a Frontiers of Fear című könyvet, a tigrisek történetéről.
Erről most nem kívánok szót ejteni, viszont elővettem újra a Pattern Languages-t is, Bernd javaslatára, neki is folyton eszébe jut velem kapcsolatban, meg nekem is.
Maga a könyv rendszere, ahogyan összefűzi a látszólagosan várostervezésről szóló kutatást egyéb mélyfilozófiai gondolatokkal.

Oldalak, amiket ki kellene nyomtatni és nézegetni:
Life Cycle: 188

UNFOLDING STORIES
The timeless way of buildings
hármas szerkezet: THE QUALITY – THE GATE – THE WAY
Jálics Ferenc: hármas szerkezet – az út…?
Fear of letting go…
84.old. Shocking, few personal life patterns


Day 18 – 05/19
Újult erő, struktúrára lelés, orvosi diagnózis szerint

Ezeket a tézisből emeltem ki, hátha kell még később:

The choice and the lines of associations are arbitrary to keep the joyfulness of the narrative.
There are different type of writings, separated with different styles.

The normal “Serif” parts

Liberation Sans parts
Where I’m using nice terminology and phrases to name phenomenons (mostly happen only in the mind).
Daily journal parts
Personal notes and sufffferings for a given picture

Pre table of content, explaining arbitrary choice of animal
Elephant related
Eating
Giving back to the nature
    • missing gap: coming back from India without any elephant
    • filling gap: creating associations with elephant to depict desired topic
    • learn it by taste it
    • anatomize elephant body because I know it much more less than human body
        ◦ kezdők szerencséje
        ◦ amatőr rálelések
        ◦ inhale in
    • nem fejezetcím Anna! Meditációs magyarázat
    • Connect outside

diagnosis for each part

Explaining writing in one page is fun!

Missing gap:
coming back from India without elephant in my pocket

Filling gap:
choosing elephant to articulate given theme arbitrary is the solution to complete and satiate my Indian hunger, which wasn’t gorged by beautiful amazing butterflies and cows in surrealistic situations.

Most, hogy elkezdtem, rátaláltam, kifejlesztettem a tényleges struktúrát, kissé bajban is vagyok, mert bele kell illesztenem a mozaikdarabkákat a kitalált kategóriákba.
Összegyűjtenem a régmúltban magamra szedett, megrágogatott jegyzeteket innen-onnan.
Mert az altémákat, amiken keresztül bemutatom a fejezeteket, ki kell bővítenem.
Ilyen például a köbö egy hónappal ezelőtt elolvasott “The Embodied Mind”. Nagyon tetszett, nagyon jó volt, de most fel kell elevenítenem, mi volt jó benne. 
Legközelebb: úgy kellene ezt, hogy amikor olvasok és jegyzetelek, akkor eleve adok a jegyzetnek egy irányt, egy kategórahalmazt, akár egy illusztrációt, hogy később felidéződjön az a kontent, ami úgy magával ragadott “akkor”.

Day 19 – 05/20
Borús eges, de új témás-írós-kitalálós,
menni kell a részletekbe...megint!

Az írás művészetéről:
Christopher Alexander videója
You mentioned me this guy, I'm remembering again and again. I know now why.
Because he is writing and categorizing in a very arbitrary way. His patterns are "valid" and make sense, but not in the practical, architectural meaning, but in the "details". I have read his "pre-book": Timeless way of buildings, and yes, it confirmed for me the following:
He wanted to talk about many aspects of the life at all, and the "covering reason" was that it is an architectural technique. I think he knew it. But he just "didn't care at all". That's why I'm so amazed of him and his book.
Telling your "story" in a way which seems that it fits into requirements but you can just playing with the writing act and with everyone.

He uses attention.
He totally aware of when he has attention and he can use it, he talks to the reader and to the audience into the same immediate way. Because he is practical. But he is a thinker as well.

The requirement is that there should be a framework, a logic guideline which can lead the reader smoothly into the direction, where he or she can decide where to go and how(?)

He pretends that he offers a book  for a practical/commercial usage, a manual, but in the reality the knowledge what you gain from it is more substantial and goes beyond itself. 

He pays attention to the user experience, he address the reader in an immediate way, gives the guiding “rules” to the readers hand, gives the pieces of the puzzle, but offers more pictures to lay out. It depends on the user.
This way of his “story telling” is enjoyable for the writer, who can suit the next desired text piece easily into the context, and find the right category, and attractive for the reader as well, who feels apparelled with an interactive role in twirling the book.

The video:
the voice of:
- He pours the water from the jag into his paper cup. It sound as there is ice cubes in the jag but probably there is none.
- He takes a gulp/sups from it.
His posture, his gestures. I like how he reclines on the desk, he touches his face or put his hand into his pocket.
It seems sometimes that he will sit on the chair but he isn’t.
The breaks he keeps between the thoughts.

Elefánt a porcelánboltban
Elephant in a porcelain shop.
Stay in touch with elephant.
When an elephant goes into the porcelain shop, he strikes down and break everything around him.
He is too much big and doesn’t be aware of his “bigness”.
Even he acts slowly, takes too much big steps and can’t see the surroundings because he doesn’t see his own size.
Can he just see himself first?
After probably he won’t break everything around.
How to approach his body?
Also this shop has many different silly objects around, and the places are changing all the time.
You have to be careful, when heading somewhere, you have to be aware of your body but the path as well, to pay attention each object every moment.


Day 20 – 05/21
Összegzős, hogy át tudjam küldeni reviewra!

Tervezett fejezetcímek:
4. Wheel of the Dharma (nagyképű)

Day 21 – 05/22
Újhold

Nézem vissza a régi jegyzeteket, írom ki, ami fontos, ami előbbre lendít.

- a tudás az elme számára
- a tapasztalat a test számára
kémialiag tanítod a tested arra, amit az elméd már megértett (contemplation, meditation)
- ismétlés szándékosan: elme & test együtt dolgozik
- összehangolod a viselkedést a szándékkal
- cselekedeteidet összehangolod a gondolataiddal
agy, gondolat → vegyület → érzelem, emóció →(feeling) → új tapasztalat + testi érzet, tünet, lenyomat
active attention & mild effort vs. passive, az BB.
“bad feelings”
tökéletlenül tökéletes ember

FIELDWORK – gardening

Latour Fictional Being -  creating attachment

Day 22 – 06/05
Telihold, tízmilliószoros nap!

Van valami furcsa az égen.
Valami furcsa képződmény.
Nem igazán tudom megmagyarázni, de egy lyuk tátong az égen, a felhők mögött süt a nap, és olyan borongós is meg olyan fényviszonyok vannak, mint napfogyatkozáskor “szokott” lenni.

Már eltelt annyi idő, mióta az étkezésemet és a nem evésemet figyelem, hogy némi gyakorlatra és használható tapasztalatra tettem szert.
Továbbra is ezzel játszom, mikor mennyit, legyen-e étvágyam, s ha igen, az jó-e és akkor mennyit és mit. Viszont nem vagyok úgy ráfeszülve a témára.
Azonban “visszacsúszni” az étkezéssel kapcsolatos gondolati zárlatkörökbe nagyon egyszerű.
Ha nincsen meg a pontos napi irány, a céltudat, akkor könnyű azon elagyalni, hogy mit kellene reggelizni.
Az utóbbi pár hétben volt olyan nap, amikor már reggelizni sem kívántam, meg úgy általában enni. És jól voltam fizikailag. Mentálisan pedig egyenesen szuperül.
Most viszont, úgy érzem:
Minden a lasagnával kezdődött… (újra)

Oké, akkor én most tényleg az étkezés/táplálkozásról fogok írni!

