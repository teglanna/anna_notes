---
id: biblio
sidebar_label:  References, keywords
title: References, keywords
---

## Nice Readers

    • Negative capability: a theory first articulated by John Keats in 1817 to characterise the ability of the greatest writers (particularly Shakespeare) when they follow an approach even it leads them into intellectual confusion and uncertainty. The term has been used later to describe the strength when someone is able to act and operate in spite of the unknown negative circumstances.
      “At once it struck me, what quality went to form a Man of Achievement, especially in literature, and which Shakespeare possessed so enormously,” he wrote. “I mean Negative Capability, that is when man is capable of being in uncertainties, mysteries, doubts, without any irritable reaching after fact and reason.”

    • Cartesian anxiety: refers to the notion that, since René Descartes posited his influential form of body-mind dualism, Western civilization has suffered from a longing for ontological certainty, or feeling that scientific methods, and especially the study of the world as a thing separate from ourselves, should be able to lead us to a firm and unchanging knowledge of ourselves and the world around us. The term is named after Descartes because of his well-known emphasis on "mind" as different from "body", "self" as different from "other". 
      http://blog.adw.org/2011/04/on-the-cartesian-anxiety-of-our-times-and-what-faith-can-offer/
    • Cognitive dissonance: refers to a situation involving conflicting attitudes, beliefs or behaviors. This produces a feeling of mental discomfort leading to an alteration in one of the attitudes, beliefs or behaviors to reduce the discomfort and restore balance. occurs when a person holds two or more contradictory beliefs, ideas, or values, or participates in an action that goes against one of these three, and experiences psychological stress because of that.
      https://www.simplypsychology.org/cognitive-dissonance.html + wikipedia

    • mental health, upeksha, intention, embodied mind, perception, reflection in it, tools for conviviality; human experience; the carthesian anxiety; common sense; premortem; wheel of the dharma, “having enough”, compassion
Gut, brain, feelings, microbes
    • gut-brain connection
    • Visceral sensation and emotion: a study using hypnosis
    • anger?
    • Don Norman:
        ◦ visceral design: https://www.interaction-design.org/literature/article/norman-s-three-levels-of-design
        ◦ books: https://jnd.org/tag/book/
        ◦ https://en.wikipedia.org/wiki/Frederick_Lenz

(Change) behaviour, perception
learn and transform feelings: stress, fear, anger → change feelings (to reduce violence)

    • cognitive dissonance, and how to resolve it
    • ted: deal with a bully
    • emptiness: Joris van Bakel intro – Eredeti Fény
    • Roland Barthes by Roland Barthes
    • Erin Manning: “Toward a leaky sense of self”
    • Illich, Ivan, “Tools for conviviality”
    • Premortem,  prospective hindsight:
        ◦ ted: How to stay calm when you know you’ll be stressed
        ◦ https://hbr.org/2007/09/performing-a-project-premortem
        ◦ https://www.theguardian.com/lifeandstyle/2014/may/10/hindsight-in-advance-premortem-this-column-change-life
        ◦ https://www.linkedin.com/pulse/prospective-hindsight-critical-leadership-life-practice-todd-zipper
          
“Body-lines”, borderline, borderline? Self, autism… 
    • The Embodied Mind
    • Despret, Vinciane, “The Body We Care For: Figures of Anthropo-zoo-genesis”
    • Hofstadter, Douglas, I am a strange loop
    • ted: Your body language may shape who you are
      
Spiritual practice, teachings
Jalics, Franz. 1996. “Contemplative Retreat: An Introduction to the Contemplative Way to Life and 	to the Jesus Prayer”
 
    • movie: Két pápa, kritika, valóság v fikció; J F interview, Jálics Ferenc, szemlélődő lelkigyakorlat; Jezsuiták, lelkigyakorlat;
    • movie: Silence, the history behind; Christian martyrs, 26 martyrs
	https://www.americamagazine.org/faith/2019/11/19/first-jesuit-arrived-japan-1549-why-are-	there-so-few-christians-today
	https://referenceworks.brillonline.com/entries/jesuit-historiography-online/studies-on-the-	jesuit-japan-mission-COM_196472

Thich Nhat Hahn:
    • interview: Thich Nhat Hahn
    • talk: how to let anger out
    • book: “No mud no lotus”