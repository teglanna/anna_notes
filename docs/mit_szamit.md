---
id: mit_szamit
title: Mit számít, ki beszél
sidebar_label: Who cares who talks
---

## Vagy: mit számít, kinek beszél /Heller Ágnes/

- kinek beszélünk - változó súllyal
- epigramma: szubjektum nélküli műfaj, narratív -> megszólító, megszólított háttérben
- aforizma: Nietzsche: megszólított fontos, megszólító + megszólított viszonya
viszony, viszonyosság
- egyén helyett gondolat autoritása

**csillagbarátság**

*"A megszólított, aki eldönti, hogy a szöveg megszólította, eldöntheti azt is, hogy mi szólította meg és mi nem."*
