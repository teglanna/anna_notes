---
id: ne_cap
sidebar_label: negative_capability
title: Negative Capability
---


A theory first articulated by John Keats in 1817 to characterise the ability of the greatest writers (particularly Shakespeare) when they follow an approach even it leads them into intellectual confusion and uncertainty. The term has been used later to describe the strength when someone is able to act and operate in spite of the unknown negative circumstances. “At once it struck me, what quality went to form a Man of Achievement, especially in literature, and which Shakespeare possessed so enormously,” he wrote. “I mean Negative Capability, that is when man is capable of being in uncertainties, mysteries, doubts, without any irritable reaching after fact and reason.”